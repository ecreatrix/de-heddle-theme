<?php
namespace App\Posts;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Blog::class ) ) {
	class Blog {
		public function __construct() {
			add_action( 'admin_menu', [$this, 'change_post_label'] );
			add_action( 'init', [$this, 'change_post_object'] );

			add_action( 'init', [$this, 'unregister_taxonomy'] );

			// Remove unnecessary columns and add excerpt
			add_filter( 'manage_post_posts_columns', [$this, 'visible_columns'] );

			add_filter( 'manage_edit-post_sortable_columns', ['App\Posts\Helpers', 'sortable_columns'] );
		}

		public function change_post_label() {
			global $menu;
			global $submenu;
			$menu[5][0]                 = 'Media Article';
			$submenu['edit.php'][5][0]  = 'Media Article';
			$submenu['edit.php'][10][0] = 'Add Media Article';
		}

		public function change_post_object() {
			global $wp_post_types;

			$labels = &$wp_post_types['post']->labels;

			$labels->name               = 'Media';
			$labels->singular_name      = 'Media';
			$labels->add_new            = 'Add Article';
			$labels->add_new_item       = 'Add Article';
			$labels->edit_item          = 'Edit Article';
			$labels->new_item           = 'Media';
			$labels->view_item          = 'View Media';
			$labels->search_items       = 'Search Media';
			$labels->not_found          = 'No article found';
			$labels->not_found_in_trash = 'No article found in Trash';
			$labels->all_items          = 'All media articles';
			$labels->menu_name          = 'Media';
			$labels->name_admin_bar     = 'Media';
		}

		public function columns_custom( $column, $post_id ) {
			switch ( $column ) {
				case 'wa_excerpt':
					// content column
					echo Helpers::excerpt( $post_id );

					break;
			}
		}

		public function sortable_columns( $columns ) {
			$columns['wa_excerpt'] = 'excerpt';

			return $columns;
		}

		public function unregister_taxonomy() {
			//unregister_taxonomy_for_object_type('category', 'post');
			//unregister_taxonomy_for_object_type( 'post_tag', 'post' );

			remove_post_type_support( 'post', 'post-formats' );
		}

		public function visible_columns( $columns ) {
			unset( $columns['author'] );
			unset( $columns['comments'] );
			$new_columns = [
				'wa_excerpt' => __( 'Excerpt' ),
			];

			// Add excerpt after 'title' column
			$columns = Helpers::insert_after( $columns, $new_columns, 'title' );

			return $columns;
		}
	}

	new Blog();
}