<?php
namespace App\Posts;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Locations::class ) ) {
    class Locations {
        public function __construct() {
            // Create CPT
            $this->create();

            // Fix dashboard columns
            add_filter( 'manage_location_posts_columns', [$this, 'visible_columns'] );
            add_action( 'manage_location_posts_custom_column', [$this, 'columns_custom'], 10, 2 );

            add_filter( 'manage_edit-location_sortable_columns', [$this, 'sortable_columns'] );
        }

        public function columns_custom( $column, $post_id ) {
            switch ( $column ) {
                case 'wa_address':
                    // content column
                    echo get_post_meta( $post_id, 'wa_address', true );

                    break;
            }
        }

        public function create() {
            $args = [
                'slug' => 'location',
            ];

            $labels = Helpers::create_labels( $args );

            $cpt_args = [
                'labels'             => $labels,
                'description'        => __( '', 'ec-theme' ),
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => ['slug' => $args['slug'] . 's', 'with_front' => false],
                'capability_type'    => 'post',
                'has_archive'        => false,
                'hierarchical'       => false,
                'menu_icon'          => 'dashicons-location-alt',
                'menu_position'      => 5,
                'supports'           => ['title', 'editor', 'page-attributes', 'excerpt', 'thumbnail'], // Editor needed for CPT to have Gutenberg
                'show_in_rest'       => true,                                                           // Must be true for CPT to have Gutenberg
            ];

            register_post_type( 'location', $cpt_args );
        }

        public function sortable_columns( $columns ) {
            $columns['wa_address'] = 'address';

            return $columns;
        }

        public function visible_columns( $columns ) {
            $new_columns = [
                'wa_address' => __( 'Address' ),
            ];

            $columns['title'] = __( 'Location' );

            // Add address after 'title' column
            $columns = Helpers::insert_after( $columns, $new_columns, 'title' );

            return $columns;
        }
    }

    new Locations();
}
