<?php
namespace App\Posts;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Testimonials::class ) ) {
    class Testimonials {
        public function __construct() {
            // Create CPT
            $this->create();

            // Fix dashboard columns
            //add_filter( 'manage_testimonial_posts_columns', [$this, 'visible_columns'] );

            // Change title content
            //add_action( 'admin_head-edit.php', [$this, 'new_title'] );
            //add_filter( 'save_post', [$this, 'new_title_filter'] );

            //add_filter( 'manage_edit-testimonial_sortable_columns', ['App\Posts\Helpers', 'sortable_columns'] );
        }

        public function create() {
            $args = [
                'slug' => 'testimonial',
            ];

            $labels = Helpers::create_labels( $args );

            $cpt_args = [
                'labels'             => $labels,
                'description'        => __( '', 'ec-theme' ),
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => ['slug' => $args['slug'] . 's', 'with_front' => false],
                'capability_type'    => 'post',
                'has_archive'        => false,
                'hierarchical'       => false,
                'menu_icon'          => 'dashicons-testimonial',
                'menu_position'      => 5,
                'supports'           => ['title', 'editor', 'excerpt', 'page-attributes', 'thumbnail'], // Editor needed for CPT to have Gutenberg
                'show_in_rest'       => true,                                                           // Must be true for CPT to have Gutenberg
            ];

            register_post_type( 'testimonial', $cpt_args );
        }

        // Update title from admin list screen
        public function new_title() {
            add_filter( 'the_title', [$this, 'new_title_content'], 100, 2 );
        }

        // Update title in specific location
        public function new_title_content( $title, $id ) {
            global $typenow;
            if ( 'testimonial' == $typenow ) {
                return Helpers::author( $id );
            }

            return $title;
        }

        // Update title across site
        public function new_title_filter( $post_id ) {
            $post_type = get_post_type( $post_id );

            // If this isn't a 'testimonial' post, don't update it.
            if ( 'testimonial' != $post_type ) {
                return;
            }
            $title = Helpers::author( $post_id );
            // unhook this function so it doesn't loop infinitely
            remove_action( 'save_post', [$this, 'new_title_filter'] );

            // update the post, which calls save_post again
            wp_update_post( ['ID' => $post_id, 'post_title' => $title, 'post_name' => $title] );

            // re-hook this function
            add_action( 'save_post', [$this, 'new_title_filter'] );
        }

        public function visible_columns( $columns ) {
            $new_columns = [
                'wa_excerpt' => __( 'Excerpt' ),
            ];

            $columns['title'] = __( 'Author' );

            // Add excerpt after 'title' column
            $columns = Helpers::insert_after( $columns, $new_columns, 'title' );

            return $columns;
        }
    }

    new Testimonials();
}
