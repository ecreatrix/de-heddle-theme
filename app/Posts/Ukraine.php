<?php
namespace App\Posts;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Ukraine::class ) ) {
	class Ukraine {
		public function __construct() {
			// Create CPT
			$this->create();

		}

		public function create() {
			$args = [
				'slug' => 'ukraine',
			];

			$labels              = Helpers::create_labels( $args );
			$labels['name']      = 'Ukraine';
			$labels['all_items'] = 'Ukraine';

			$cpt_args = [
				//'labels'             => $labels,
				'label'              => 'Ukraine',
				'description'        => __( '', 'ec-theme' ),
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'rewrite'            => ['slug' => $args['slug'], 'with_front' => false],
				'capability_type'    => 'post',
				'has_archive'        => false,
				'hierarchical'       => false,
				'menu_icon'          => 'dashicons-admin-site',
				'menu_position'      => 5,
				'supports'           => ['title', 'editor', 'page-attributes', 'thumbnail', 'excerpt'], // Editor needed for CPT to have Gutenberg
				'show_in_rest'       => true,                                                           // Must be true for CPT to have Gutenberg
			];

			register_post_type( 'ukraine', $cpt_args );
		}
	}

	new Ukraine();
}