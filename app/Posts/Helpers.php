<?php
namespace App\Posts;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Helpers::class ) ) {
	class Helpers {
		public function __construct() {
			// Used by multiple post types
			add_action( 'manage_posts_custom_column', [$this, 'columns_custom'], 10, 2 );
			add_filter( 'manage_posts-sortable_columns', [$this, 'sortable_columns'] );
		}

		public static function author( $post_id ) {
			$name     = get_post_meta( $post_id, 'wa_author_name', true );
			$company  = get_post_meta( $post_id, 'wa_author_company', true );
			$position = get_post_meta( $post_id, 'wa_author_position', true );

			if ( $name && $company ) {
				$company = ' - ' . $company;
			}
			if ( $position && ( $name || $company ) ) {
				$position = ', ' . $position;
			}

			return $name . $company . $position;
		}

		// Create labels from name, slug and menu_name. Only slug necessary
		public static function create_labels( $args ) {
			if ( ! $args || ! array_key_exists( 'slug', $args ) ) {
				return [];
			}

			$args['name'] = ucfirst( $args['slug'] );

			if ( ! array_key_exists( 'menu_name', $args ) || ! $args['menu_name'] ) {
				$args['menu_name'] = $args['name'] . 's';
			}

			$labels = [
				'name'               => __( $args['name'] . 's', 'post type general name', 'ec-theme' ),
				'singular_name'      => __( $args['name'], 'post type singular name', 'ec-theme' ),
				'menu_name'          => __( $args['menu_name'], 'admin menu', 'ec-theme' ),
				'name_admin_bar'     => __( $args['name'], 'add new on admin bar', 'ec-theme' ),
				'add_new'            => __( 'Add New', $args['slug'], 'ec-theme' ),
				'add_new_item'       => __( 'Add New ' . $args['name'], 'ec-theme' ),
				'new_item'           => __( 'New ' . $args['name'], 'ec-theme' ),
				'edit_item'          => __( 'Edit ' . $args['name'], 'ec-theme' ),
				'view_item'          => __( 'View ' . $args['name'], 'ec-theme' ),
				'all_items'          => __( 'All ' . $args['name'] . 's', 'ec-theme' ),
				'search_items'       => __( 'Search ' . $args['name'] . 's', 'ec-theme' ),
				'parent_item_colon'  => __( 'Parent ' . $args['name'] . 's:', 'ec-theme' ),
				'not_found'          => __( 'No ' . $args['slug'] . 's found.', 'ec-theme' ),
				'not_found_in_trash' => __( 'No ' . $args['slug'] . 's found in Trash.', 'ec-theme' ),
			];

			return $labels;
		}

		public static function excerpt( $post_id ) {
			// content column
			$excerpt = get_the_excerpt( $post_id );

			if ( ! has_excerpt( $post_id ) ) {
				$excerpt = get_the_content( $post_id );
			}

			return self::string_remove_after_word( $excerpt, 125 );
		}

		// Add $new_array to $columns array after $key index
		public static function insert_after( $columns, $new_array, $key = 'title' ) {
			if ( ! is_array( $columns ) || ! is_array( $new_array ) ) {
				return $columns;
			}

			$index   = array_search( $key, array_keys( $columns ) ) + 1;
			$columns = array_slice( $columns, 0, $index, true ) + $new_array + array_slice( $columns, $index, null, true );

			return $columns;
		}

		public static function sortable_columns( $columns ) {
			$columns['wa_excerpt'] = 'excerpt';

			return $columns;
		}

		// Remove leftover letters by character count
		public static function string_remove_after_character( $excerpt, $max_words = false, $second_occurrence = false, $strip_tags = true, $append = '...' ) {
			$excerpt = self::string_shrink( $excerpt, $max_words, $second_occurrence, $strip_tags, $append, false );

			return $excerpt;
		}

		// Remove leftover letters by word count
		public static function string_remove_after_word( $excerpt, $max_words = false, $second_occurrence = false, $strip_tags = true, $append = '.' ) {
			$excerpt = self::string_shrink( $excerpt, $max_words, $second_occurrence, $strip_tags, $append, true );

			return $excerpt;
		}

		public static function string_shrink( $excerpt, $max_length = false, $second_occurrence = false, $strip_tags = true, $append = '...', $limit_words = true ) {
			if ( $strip_tags ) {
				$excerpt = strip_tags( $excerpt, '<p>' );
			}

			// Shrink to max_length
			$length     = strlen( $excerpt );
			$breakpoint = $length;

			if ( $max_length && 0 != $max_length && $length > $max_length ) {
				if ( $limit_words ) {
					// Limit by number of words
					$needle = '.';
				} else {
					// Limit by number of characters
					$needle = ' ';
				}

				$breakpoint = strpos( $excerpt, $needle, $max_length );
				if ( $second_occurrence ) {
					//TODO FIX
					$breakpoint = strpos( $excerpt, $needle, $breakpoint + strlen( $needle ) );
				}
			}

			if ( $breakpoint < $length - 1 ) {
				$excerpt = trim( substr( $excerpt, 0, $breakpoint ) ) . $append;
			}

			return $excerpt;
		}
	}
}