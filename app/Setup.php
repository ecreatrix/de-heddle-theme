<?php
namespace App;
use function Roots\bundle;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Setup::class ) ) {
	class Setup {
		public function __construct() {
			add_filter( 'admin_footer_text', [$this, 'dashboard_footer'] );
			add_action( 'customize_register', [$this, 'customizer'] );
			add_filter( 'customize_loaded_components', 'customizer_remove' );
			add_action( 'after_setup_theme', [$this, 'generic'], 20 );
			add_filter( 'excerpt_more', [$this, 'more'] );

			//add_action( 'widgets_init', [$this, 'needed_to_show_customizer'] );
		}

		public function customizer( \WP_Customize_Manager $wp_customize ) {
			// Add Theme Options Panel
			$wp_customize->add_panel( 'wa_option_panel', [
				'title'      => esc_html__( 'Heddle Shipyards', 'wa-theme' ),
				'priority'   => 20,
				'capability' => 'edit_theme_options',
			] );

			$wp_customize->add_section( 'wa_social', [
				'title'       => esc_html__( 'Social Media & Contact', 'wa-theme' ),
				'description' => __( 'Contact info and social media links', 'wa-theme' ),
				'priority'    => 110,
				'capability'  => 'edit_theme_options',
				'panel'       => 'wa_option_panel',
			] );

			// Add options
			$wp_customize->add_setting(
				'wa_email', [
					'sanitize_callback' => 'sanitize_email', //removes all invalid characters
				] );
			$wp_customize->add_control(
				'wa_email', [
					'label'   => esc_html__( 'Email', 'wa-theme' ),
					'section' => 'wa_social',
					'type'    => 'email',
				] );

			$wp_customize->add_setting(
				'wa_phone', [
					'sanitize_callback' => 'wp_filter_nohtml_kses', //removes all HTML from content
				] );
			$wp_customize->add_control(
				'wa_phone', [
					'label'   => esc_html__( 'Phone Number', 'wa-theme' ),
					'section' => 'wa_social',
					'type'    => 'text',
				] );

			$wp_customize->add_setting(
				'wa_fax', [
					'sanitize_callback' => 'wp_filter_nohtml_kses', //removes all HTML from content
				] );
			$wp_customize->add_control(
				'wa_fax', [
					'label'   => esc_html__( 'Fax Number', 'wa-theme' ),
					'section' => 'wa_social',
					'type'    => 'text',
				] );

			$wp_customize->add_setting(
				'wa_instagram', [
					'sanitize_callback' => 'esc_url_raw', //cleans URL from all invalid characters
				] );
			$wp_customize->add_control(
				'wa_instagram', [
					'label'   => esc_html__( 'Instagram', 'wa-theme' ),
					'section' => 'wa_social',
					'type'    => 'url',
				] );

			$wp_customize->add_setting(
				'wa_twitter', [
					'sanitize_callback' => 'esc_url_raw', //cleans URL from all invalid characters
				] );
			$wp_customize->add_control(
				'wa_twitter', [
					'label'   => esc_html__( 'Twitter', 'wa-theme' ),
					'section' => 'wa_social',
					'type'    => 'url',
				] );

			$wp_customize->add_setting(
				'wa_linkedin', [
					'sanitize_callback' => 'esc_url_raw', //cleans URL from all invalid characters
				] );
			$wp_customize->add_control(
				'wa_linkedin', [
					'label'   => esc_html__( 'LinkedIn', 'wa-theme' ),
					'section' => 'wa_social',
					'type'    => 'url',
				] );

			$wp_customize->add_setting(
				'wa_facebook', [
					'sanitize_callback' => 'esc_url_raw', //cleans URL from all invalid characters
				] );
			$wp_customize->add_control(
				'wa_facebook', [
					'label'   => esc_html__( 'Facebook', 'wa-theme' ),
					'section' => 'wa_social',
					'type'    => 'url',
				] );

			$wp_customize->add_setting(
				'wa_youtube', [
					'sanitize_callback' => 'esc_url_raw', //cleans URL from all invalid characters
				] );
			$wp_customize->add_control(
				'wa_youtube', [
					'label'   => esc_html__( 'YouTube', 'wa-theme' ),
					'section' => 'wa_social',
					'type'    => 'url',
				] );

			$wp_customize->remove_control( 'custom_css' );
		}

		function customizer_remove( $components ) {
			$i = array_search( 'widgets', $components );
			if ( false !== $i ) {
				unset( $components[$i] );
			}
			return $components;
		}

		public function dashboard_footer() {
			echo 'Built for you by <a href="https://detale.ca/" target="_blank">Detale</a> :)';
		}

		public function generic() {
			/**
			 * Enable features from the Soil plugin if activated.
			 * @link https://roots.io/plugins/soil/
			 */
			add_theme_support( 'soil', [
				'clean-up',
				'nav-walker',
				'nice-search',
				'relative-urls',
			] );

			add_filter( 'body_class', function ( $classes ) {
				$classes[] = 'd-flex flex-column h-100';

				if ( BlockHelpers::has_hero() ) {
					$classes[] = 'has-hero';
				}

				return $classes;
			} );

			/**
			 * Disable full-site editing support.
			 *
			 * @link https://wptavern.com/gutenberg-10-5-embeds-pdfs-adds-verse-block-color-options-and-introduces-new-patterns
			 */
			remove_theme_support( 'block-templates' );

			/**
			 * Register the navigation menus.
			 * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
			 */
			register_nav_menus( [
				'primary_navigation' => __( 'Primary Navigation', 'wa-theme' ),
				'footer_navigation'  => __( 'Footer Navigation', 'wa-theme' ),
			] );

			/**
			 * Disable the default block patterns.
			 * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#disabling-the-default-block-patterns
			 */
			remove_theme_support( 'core-block-patterns' );

			/**
			 * Enable plugins to manage the document title.
			 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
			 */
			add_theme_support( 'title-tag' );

			/**
			 * Enable post thumbnail support.
			 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
			 */
			add_theme_support( 'post-thumbnails' );

			/**
			 * Enable responsive embed support.
			 * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#responsive-embedded-content
			 */
			add_theme_support( 'responsive-embeds' );

			add_theme_support( 'align-full' );

			/**
			 * Enable HTML5 markup support.
			 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
			 */
			add_theme_support( 'html5', [
				'caption',
				'comment-form',
				'comment-list',
				'gallery',
				'search-form',
				'script',
				'style',
			] );

			/**
			 * Enable selective refresh for widgets in customizer.
			 * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
			 */
			add_theme_support( 'customize-selective-refresh-widgets' );

			$defaults = [
				'height'               => 100,
				'width'                => 400,
				'flex-height'          => true,
				'flex-width'           => true,
				'header-text'          => ['site-title', 'site-description'],
				'unlink-homepage-logo' => true,
			];

			add_theme_support( 'custom-logo', $defaults );
		}

		public function more() {
			return sprintf( ' &hellip; <a href="%s">%s</a>', get_permalink(), __( 'Continued', 'wa-theme' ) );
		}

		function needed_to_show_customizer() {
			register_sidebar( [
				'name' => __( 'Footer', 'sage' ),
				'id'   => 'sidebar-footer',
			] );
		}
	}

	new Setup();
}