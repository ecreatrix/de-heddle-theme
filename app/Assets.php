<?php
namespace App;

use function Roots\asset;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Assets::class ) ) {
    class Assets {
        public function __construct() {
            add_action( 'wp_enqueue_scripts', [$this, 'frontend'] );

            add_action( 'enqueue_block_editor_assets', [$this, 'blocks'] );
            //add_action( 'wp_enqueue_scripts', [$this, 'editor'] );

            add_action( 'admin_enqueue_scripts', [$this, 'admin'], 999 );
            add_action( 'login_enqueue_scripts', [$this, 'admin'], 999 );
        }

        // Enqueue local and remote assets
        public function add_assets( $assets = false, $type = false ) {
            if ( ! $assets || ! is_array( $assets ) ) {
                return;
            }

            foreach ( $assets as $item ) {
                // Name required, skip if it is not present
                if ( ! array_key_exists( 'name', $item ) ) {
                    continue;
                }

                $name  = $item['name'];
                $asset = null;

                $link = $name;
                if ( array_key_exists( 'link', $item ) ) {
                    $link = $item['link'];
                }

                // Figure out if it's a script or a style
                $script = false;
                $style  = false;
                if ( 'js' === $type || strpos( $link, '.js' ) !== false ) {
                    $script = true;
                } else if ( 'css' === $type || strpos( $link, '.css' ) !== false ) {
                    $style = true;
                }

                // Get local file or remote link
                if ( strpos( $link, 'http' ) !== false ) {
                    $asset = $link;
                } else if ( $style ) {
                    $asset = 'public/styles/' . $link . '.css';
                } else if ( $script ) {
                    $asset = 'public/scripts/' . $link . '.js';
                }

                // Set version for cache busting
                $version     = null;
                $path_prefix = get_template_directory() . '/';
                $uri_prefix  = get_stylesheet_directory_uri() . '/';
                if ( file_exists( $path_prefix . $asset ) ) {
                    $version = filemtime( $path_prefix . $asset );
                    $uri     = $uri_prefix . $asset;
                } else if ( array_key_exists( 'link', $item ) ) {
                    $uri = $item['link'];
                } else {
                    return;
                }

                $dependencies = [];
                if ( array_key_exists( 'dependencies', $item ) && is_array( $item['dependencies'] ) ) {
                    $dependencies = $item['dependencies'];
                }

                // Add prefix before name
                if ( ! array_key_exists( 'prefix', $item ) ) {
                    $item['prefix'] = 'wa-theme-';
                }

                $name = $item['prefix'] . $name;

                // Add parameters inline for use by scripts
                if ( array_key_exists( 'script_object', $item ) && array_key_exists( 'parameters', $item ) ) {
                    wp_localize_script( $name, $item['script_object'], $item['parameters'] );
                }

                // Enqueue file/link
                if ( $script ) {
                    wp_enqueue_script( $name, $uri, $dependencies, $version, true );
                } else if ( $style ) {
                    wp_enqueue_style( $name, $uri, $dependencies, $version );
                }
            }
        }

        public function admin() {
            $styles = [
                [
                    'name' => 'admin',
                ],
                $this->font_fontawesome(),
                $this->font_google(),
            ];

            $this->add_assets( $styles, 'css' );
        }

        public function blocks() {
            $scripts = [
                [
                    'name'         => 'blocks',
                    'dependencies' => ['lodash', 'wp-a11y', 'wp-api-fetch', 'wp-block-editor', 'wp-block-library', 'wp-blocks', 'wp-components', 'wp-compose', 'wp-core-data', 'wp-data', 'wp-data-controls', 'wp-editor', 'wp-element', 'wp-hooks', 'wp-i18n', 'wp-keyboard-shortcuts', 'wp-keycodes', 'wp-media-utils', 'wp-notices', 'wp-plugins', 'wp-polyfill', 'wp-primitives', 'wp-url', 'wp-viewport'], //, 'wa-gutenberg-blocks.vendor-js'
                ],
            ];

            $this->add_assets( $scripts, 'js' );

            $styles = [
                [
                    'name' => 'editor',
                ],
                $this->font_fontawesome(),
                $this->font_google(),
            ];

            $this->add_assets( $styles, 'css' );
        }

        public function bootstrap( $bundle = false ) {
            $name = 'library/bootstrap';
            if ( $bundle ) {
                $name .= '.bundle';
            }

            $script = [
                'name'         => 'bootstrap',
                'link'         => $name . '.min',
                'prefix'       => 'wa-',
                'dependencies' => ['jquery'],
            ];

            return $script;
        }

        public function editor() {
            $scripts = [
                $this->font_fontawesome(),
            ];

            $this->add_assets( $scripts, 'js' );

            $styles = [
                [
                    'name' => 'editor',
                ],
                $this->font_google(),
            ];

            $this->add_assets( $styles, 'css' );
        }

        public function font_fontawesome() {
            $fontawesome = [
                'name'   => 'fontawesome',
                'prefix' => 'wa-',
                'link'   => 'https://kit.fontawesome.com/d86202f74c.js',
            ];

            return $fontawesome;
        }

        public function font_google() {
            $google = [
                'name' => 'google-font',
                'link' => 'https://fonts.googleapis.com/css?family=Roboto:300,300i,500,700,700i&display=swap',
            ];

            return $google;
        }

        public function frontend() {
            if ( is_single() && comments_open() && get_option( 'thread_comments' ) ) {
                wp_enqueue_script( 'comment-reply' );
            }

            $scripts = [
                'main' => [
                    'name'         => 'app',
                    'dependencies' => ['jquery', 'wa-bootstrap'],
                ],
                $this->font_fontawesome(),
                $this->bootstrap( true ),
            ];

            $this->add_assets( $scripts, 'js' );

            $styles = [
                [
                    'name' => 'app',
                ],
                $this->font_google(),
            ];

            $this->add_assets( $styles, 'css' );
        }

        public function popper() {
            $script = [
                'name'   => 'popper',
                'prefix' => 'wa-',
                'link'   => 'library/popper.min',
            ];

            return $script;
        }
    }

    new Assets();
}