<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;

class Post extends Composer {
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'partials.page-header',
        'partials.content',
        'partials.content-*',
    ];

    public function date() {
        return get_the_date();
    }

    /**
     * Data to be passed to view before rendering, but after merging.
     *
     * @return array
     */
    public function override() {
        return [
            'title' => $this->title(),
            'date'  => $this->date(),
        ];
    }

    public static function post_nav() {
        if ( ! is_single() || 'post' != get_post_type() ) {
            return false;
        }

        $archive_link = '/media/news';
        $category     = get_the_category();

        if ( is_array( $category ) && ! empty( $category ) && $category[0] ) {
            $name = $category[0]->category_nicename;
            //echo '<div style="display: none">' . print_r( $category, true ) . '</div>';
            //echo '<div style="display: none">' . $name . '</div>';

            if ( 'press-releases' === $name ) {
                $title        = 'Press Releases';
                $archive_link = '/media/press-releases';
            } else if ( 'publications' === $name ) {
                $title        = 'Publications';
                $archive_link = '/media/publications';
            }
        }

        $word_limit = 8;
        $title      = '';

        $previous      = get_previous_post();
        $previous_link = false;
        if ( $previous ) {
            //$title         = ': '.Helpers::string_remove_after_word($previous->post_title, $word_limit, '...', true);
            //$previous_link = '<div class="col previous skip"><i class="fa fas fa-chevron-left"></i><div class="link"><a href="' . get_permalink( $previous ) . '">Previous' . $title . '</a></div></div>';
            $previous_link = get_permalink( $previous );
        }

        //$archive_link = '<div class="col archive skip"><div class="link"><a href="' . $archive_link . '">All ' . $title . '</a></div></div>';

        $title     = '';
        $next      = get_next_post();
        $next_link = false;
        if ( $next ) {
            //$title     =': '.Helpers::string_remove_after_word($next->post_title, $word_limit, '...', true);
            //$next_link = '<div class="col next skip"><div class="link"><a href="' . get_permalink( $next ) . '">Next' . $title . '</a></div><i class="fa fas fa-chevron-right"></i></div>';
            $next_link = get_permalink( $next );
        }

        $title = 'News';

        return [
            'title'         => $title,
            'archive_link'  => $archive_link,
            'previous_link' => $previous_link,
            'next_link'     => $next_link,
        ];

        return '<nav class="jumbotron bg-grey-100" aria-label="Page navigation"><div class="container"><div class="pagination row">' . $previous_link . $archive_link . $next_link . '</div></div></nav>';
    }

    /**
     * Returns the post title.
     *
     * @return string
     */
    public function title() {
        if ( $this->view->name() !== 'partials.page-header' ) {
            return get_the_title();
        }

        if ( is_home() ) {
            if ( $home = get_option( 'page_for_posts', true ) ) {
                return get_the_title( $home );
            }

            return __( 'Latest Posts', 'wa-theme' );
        }

        if ( is_archive() ) {
            return get_the_archive_title();
        }

        if ( is_search() ) {
            return sprintf(
                /* translators: %s is replaced with the search query */
                __( 'Search Results for %s', 'wa-theme' ),
                get_search_query()
            );
        }

        if ( is_404() ) {
            return __( 'Not Found', 'wa-theme' );
        }

        return get_the_title();
    }

    public function with() {
        return [
            'nav' => $this->post_nav(),
        ];
    }
}
