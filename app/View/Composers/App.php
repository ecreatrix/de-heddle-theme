<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;

class App extends Composer {
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        '*',
    ];

    /**
     * Returns the site name.
     *
     * @return string
     */
    public function siteName() {
        return get_bloginfo( 'name', 'display' );
    }

    public static function social_media() {
        $social_info = [
            'linkedin'  => [
                'name' => 'LinkedIn',
                'icon' => 'linkedin-square',
                'link' => get_theme_mod( 'wa_linkedin' ),
            ],
            'instagram' => [
                'name' => 'Instagram',
                'icon' => 'instagram',
                'link' => get_theme_mod( 'wa_instagram' ),
            ],
            'twitter'   => [
                'name' => 'Twitter',
                'icon' => 'twitter-square',
                'link' => get_theme_mod( 'wa_twitter' ),
            ],
            'facebook'  => [
                'name' => 'Facebook',
                'icon' => 'facebook-square',
                'link' => get_theme_mod( 'wa_facebook' ),
            ],
            'youtube'   => [
                'name' => 'YouTube',
                'icon' => 'youtube-square',
                'link' => get_theme_mod( 'wa_youtube' ),
            ],
        ];

        return $social_info;
    }

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return [
            'siteName' => $this->siteName(),
            'social'   => $this->social_media(),
        ];
    }
}
