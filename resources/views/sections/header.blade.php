<header id="masthead">
	<nav class="sidebar-wrapper"><div class="sidebar sidebar-sticky navbar navbar-expand-md navbar-light">
		<a class="navbar-brand" href="{{ home_url( '/' ) }}">
			@if( has_custom_logo( ) ) 
				{!! App\BlockHelpers::svg_or_file_return( get_theme_mod( 'custom_logo' ), $siteName ) !!}
			@else
				{!! $siteName !!}
			@endif
		</a>

		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#primaryNavbar" aria-controls="primaryNavbar" aria-expanded="false" aria-label="Toggle navigation">
			<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 30 30'><path stroke='black' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/></svg>
		</button>

		@if ( has_nav_menu( 'primary_navigation' ) )
			<div class="collapse navbar-collapse" id="primaryNavbar">
				{!! wp_nav_menu( ['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'echo' => false] ) !!}
		 	</div>
	 	@endif
	</div></nav>
</header>
