<footer id="mastfooter" class="jumbotron"><div class="container">
	@if( $social )
		<ul class="social-media navbar nav">
			@foreach( $social as $key => $info ) 
				<li class="nav-item link-{{ $key }}">
					<a class="nav-link" href="{{ $info['link'] }}" target="_blank" alt="{{ $info['name'] }} link">
						<i class="fa fa-{{ $info['icon'] }}" aria-hidden="true"></i>
					</a>
				</li>
			@endforeach
		</ul>
	@endif

	@if ( has_nav_menu( 'footer_navigation' ) )
		<div class="links">
			{!! wp_nav_menu( ['theme_location' => 'footer_navigation', 'menu_class' => 'navbar nav', 'echo' => false] ) !!}
		</div>
	@endif

	<div class="text-center copywrite">
		<span class="copy">&copy; {{ date( 'Y' ) }}</span>
		<span class="name">{{ get_bloginfo( 'name' ) }}</span>
		<span class="sep">|</span>
		<span class="tagline">{{ get_bloginfo( 'description' ) }}</span>
	</div>
</div></footer>
