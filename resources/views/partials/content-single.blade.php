@if( has_post_thumbnail() ) 
	<div class="jumbotron post-image bg-media-image" style="background-image:url(@php( the_post_thumbnail_url( get_the_id() ) ) )"></div>
@endif

@if('post' == get_post_type() || 'ukraine' == get_post_type())
<div class="jumbotron page-title"><div class="container">
	@if('ukraine' == get_post_type())
		<small class="mb-0">{{ $date }}</small>
	@endif

	<h1 class="title no-lines h2 mb-0">{{ $title }}</h1>
</div></div>
@endif

@if('post' == get_post_type())
<div class="post-content">
	@if(has_tag('full'))
		<div class="full">
	@else
		<div class="columns-2"><div class="jumbotron"><div class="container">
	@endif
@endif

@php(the_content())

@if('post' == get_post_type())
	@if(has_tag('full'))
		</div>
	@else
		</div></div></div></div>
	@endif
@endif

@if( $nav ) 
<nav class="jumbotron bg-grey100 pagination" aria-label="Page navigation"><div class="container">
	<div class="row">
		@if( $nav['previous_link'] )
			<div class="col previous skip">
				<i class="fa fas fa-chevron-left" aria-hidden="true"></i>
				<div class="link"><a href="{{ $nav['previous_link'] }}">Previous</a></div>
			</div>
		@endif
		<div class="col archive skip @if( $nav['previous_link'] )has-previous @endif @if( $nav['next_link'] )has-next @endif">
			<div class="link">
			<a href="{{ $nav['archive_link'] }}">All {{ $nav['title'] }}</a></div>
		</div>
		@if( $nav['next_link'] )
			<div class="col next skip">
				<div class="link"><a href="{{ $nav['next_link'] }}">Next</a></div>
				<i class="fa fas fa-chevron-right" aria-hidden="true"></i>
			</div>
		@endif
	</div>
</div></nav>
@endif