<div class="app flex-shrink-0">
  @include('sections.header')

  <main id="main" @php(post_class())>
    @yield('content')
  </main>

  @hasSection('sidebar')
    <aside class="sidebar">
      @yield('sidebar')
    </aside>
  @endif
</div>

@include('sections.footer')